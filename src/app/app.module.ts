import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { TopNavigationComponent } from './components/common/top-navigation/top-navigation.component';
import { LandingShowcaseComponent } from './components/pages/landing/landing-showcase/landing-showcase.component';
import { LandingAboutMeComponent } from './components/pages/landing/landing-about-me/landing-about-me.component';
import { ConnectWithMeComponent } from './components/pages/landing/connect-with-me/connect-with-me.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    TopNavigationComponent,
    LandingShowcaseComponent,
    LandingAboutMeComponent,
    ConnectWithMeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
